# M08 Loopback API
A simple API using LoopBack.

## How to use

Open a command window in your c:\44563\m08\M08 folder.

Run npm install to install all the dependencies in the package.json file.

Run 'node .' to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node .
```

Point your browser to `http://localhost:3000/explorer`. 